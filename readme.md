## This tool helps you manage and organize your downloads from Booth effortlessly. It does a few useful things to keep your downloads tidy:

- **Automatic Organization**: Downloads are sorted into folders based on your preferences.
- **File Renaming**: Makes sure your files have clear names that match their thumbnails.
- **ZIP Compression**: Packs multiple files into one ZIP for easier storage.
- **Cleanup and Flattening**: Keeps things neat by moving files around as needed.

## Getting Started

1. Clone this repo:
   ```bash
   git clone https://github.com/Synzzz/booth-dl.git
   cd booth-dl
   ```
   

## Setting Up

1. **config.yaml**: Set your watch and download folders.
2. **folders.json**: Map organizing folder names to their Booth IDs.

### Example `config.yaml`
# ! IMPORTANT ! The folders you set up here should be new empty folders because their content will be moved and deleted by the script.
```yaml
watch_folder: "path/to/watch/folder"
download_folder: "path/to/download/folder"
```

### Example `folders.json`

```json
{
    "1": "Selestia",
    "2": "Kikyo",
    "3": "Full set",
    "4": "Manuka"
}

```
Needs python3 (I was running it with Python 3.12.4)


# The annoying part
## Setting up JDownloader:

### Enable Folderwatch:

1. Open JDownloader and go to **Settings** > **Extension Modules**.
2. Check the box to enable Folderwatch.
3. Click on the new Folder Watch submenu in settings.
4. Add the folders where the download links will be sent with the FolderWatch: Folders parameter. The folder path has to be JSON formatted, for example:
   ```json
   [
    "C:\\Users\\Username\\Desktop\\booth-dl\\Folderwatch"
   ]
   ```
5. Go to **Advanced Settings** and change these settings (there's a search bar at the top):

   - **LinkGrabberSettings:** Linkgrabber Auto Confirm - enable.
   - **LinkGrabberSettings:** Default On Added Dupes Links Action - add them, too.
   - **LinkGrabberSettings:** Auto Extraction - disable
   - **GeneralSettings:** Auto Start Download Option - ALWAYS.

## If the auto download doesn't start:

1. **Set Up Packagizer Rules:**
   - Navigate to **Settings** > **Packagizer** in JDownloader.
   - Click on the **New Rule** button.
   - Enable "Matches on any File or Link and ignores conditions below".
   - Enable Auto Confirm, Auto Start Download, Auto Forced Download Start.
   - Save the new rule.


Run setup_and_run.bat and follow the instructions shown.



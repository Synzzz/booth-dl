@echo off
setlocal

REM Get the directory where the script is located and change to that directory
cd /d %~dp0

REM Check for Python installation
python --version
IF %ERRORLEVEL% NEQ 0 (
    echo Python is not installed. Please install Python and try again.
    pause
    exit /b 1
)

REM Create a virtual environment in the current directory
echo Creating virtual environment...
python -m venv venv

REM Activate the virtual environment
echo Activating virtual environment...
call venv\Scripts\activate

REM Check if requirements.txt exists
IF NOT EXIST requirements.txt (
    echo requirements.txt not found. Creating a default requirements.txt file...
    echo beautifulsoup4==4.9.3 > requirements.txt
    echo requests==2.25.1 >> requirements.txt
    echo PyYAML==5.3.1 >> requirements.txt
)

REM Install the required packages
echo Installing required packages...
pip install -r requirements.txt

REM Start JDownloader 2.0 dynamically for the current user
set JD_PATH=C:\Users\%USERNAME%\AppData\Local\JDownloader 2.0\JDownloader2.exe
if exist "%JD_PATH%" (
    echo Starting JDownloader 2.0 minimized...
    start "" "%JD_PATH%" -m
) else (
    echo JDownloader 2.0 executable not found at "%JD_PATH%".
    pause
)


REM Run the crawljob-creator.py script
echo Running crawljob-creator.py...
python crawljob-creator.py
IF %ERRORLEVEL% NEQ 0 (
    echo Error occurred while running crawljob-creator.py.
    pause
)

REM Deactivate the virtual environment
echo Deactivating virtual environment...
call venv\Scripts\deactivate.bat



pause

import os
import shutil
import zipfile
import logging

# Setup logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

def process_folders(download_folder, folder_mapping, cleanup_folder, organising):
    logging.info("Processing folders in root: %s", download_folder)
    try:
        for collection_folder in os.listdir(download_folder):
            full_collection_path = os.path.join(download_folder, collection_folder)

            # Skip the cleanup folder
            if collection_folder == os.path.basename(cleanup_folder):
                logging.debug("Skipping cleanup folder: %s", cleanup_folder)
                continue
            
            # Check if collection_folder is a directory
            if os.path.isdir(full_collection_path):
                logging.info("Processing collection folder: %s", collection_folder)
                
                if organising:
                    # Walk collection folder
                    for booth_folder in os.listdir(full_collection_path):
                        full_booth_path = os.path.join(full_collection_path, booth_folder)
                        if booth_folder.isdigit() and len(booth_folder) == 7:
                            logging.info("Found boothId folder: %s", booth_folder)
                            handle_booth_folder(full_booth_path, full_collection_path, cleanup_folder)
                else:
                    # Walk root folder
                    for booth_folder in os.listdir(download_folder):
                        full_booth_path = os.path.join(download_folder, booth_folder)
                        if booth_folder.isdigit() and len(booth_folder) == 7:
                            logging.info("Found boothId folder: %s", booth_folder)
                            handle_booth_folder(full_booth_path, download_folder, cleanup_folder)
            else:
                logging.warning("Expected folder, but found file: %s", full_collection_path)

    except Exception as e:
        logging.error("Error processing folders: %s", str(e))


def handle_booth_folder(booth_folder_path, destination_folder, cleanup_folder):
    try:
        # Find the thumbnail
        thumbnail_file = find_thumbnail(booth_folder_path)
        if not thumbnail_file:
            logging.warning("No thumbnail found in: %s", booth_folder_path)
            return
        
        # Compress files if needed
        zip_file = compress_and_rename(booth_folder_path, thumbnail_file)
        
        # Move thumbnail and zip
        move_files(thumbnail_file, zip_file, destination_folder)
        
        # Move booth folder to cleanup
        move_to_cleanup(booth_folder_path, cleanup_folder)
    
    except Exception as e:
        logging.error("Error handling booth folder %s: %s", booth_folder_path, str(e))


def find_thumbnail(booth_folder_path):
    logging.info("Finding thumbnail in: %s", booth_folder_path)
    for file_name in os.listdir(booth_folder_path):
        if file_name.endswith(".jpg") and booth_folder_path.split(os.sep)[-1] in file_name:
            logging.info("Thumbnail found: %s", file_name)
            return os.path.join(booth_folder_path, file_name)
    return None


def compress_and_rename(booth_folder_path, thumbnail_file):
    logging.info("Compressing files in: %s", booth_folder_path)
    zip_name = thumbnail_file.replace('.jpg', '.zip')
    zip_path = os.path.join(booth_folder_path, zip_name)
    
    # Find non-thumbnail files
    files_to_zip = [f for f in os.listdir(booth_folder_path) if not f.endswith('.jpg')]
    
    if len(files_to_zip) == 1:
        # If there's only one file and it's already a zip, rename it and skip zipping
        single_file = files_to_zip[0]
        single_file_path = os.path.join(booth_folder_path, single_file)
        
        if single_file.endswith('.zip'):
            logging.info("Single file is already a zip: %s, renaming to: %s", single_file, zip_name)
            shutil.move(single_file_path, zip_path)
        else:
            logging.info("Only one non-thumbnail file, renaming it to: %s", zip_name)
            shutil.move(single_file_path, zip_path)
    else:
        # Create a new zip if there are multiple files
        with zipfile.ZipFile(zip_path, 'w') as zipf:
            for file_name in files_to_zip:
                file_path = os.path.join(booth_folder_path, file_name)
                zipf.write(file_path, arcname=file_name)
                logging.info("Zipped file: %s", file_name)

    return zip_path



def move_files(thumbnail_file, zip_file, destination_folder):
    logging.info("Moving files to: %s", destination_folder)
    
    try:
        # Ensure destination folder exists
        if not os.path.exists(destination_folder):
            os.makedirs(destination_folder)
        
        # Move thumbnail
        shutil.move(thumbnail_file, os.path.join(destination_folder, os.path.basename(thumbnail_file)))
        logging.info("Moved thumbnail: %s", thumbnail_file)
        
        # Move zip
        shutil.move(zip_file, os.path.join(destination_folder, os.path.basename(zip_file)))
        logging.info("Moved zip file: %s", zip_file)
    
    except FileExistsError as fe:
        logging.warning("File already exists in destination: %s", str(fe))


def move_to_cleanup(booth_folder_path, cleanup_folder):
    logging.info("Moving booth folder to cleanup: %s", booth_folder_path)
    try:
        destination_path = os.path.join(cleanup_folder, os.path.basename(booth_folder_path))
        
        if os.path.exists(destination_path):
            logging.warning("Folder already exists in cleanup: %s. Merging content.", destination_path)
            # Move each file from the source to the existing folder
            for item in os.listdir(booth_folder_path):
                source_item_path = os.path.join(booth_folder_path, item)
                destination_item_path = os.path.join(destination_path, item)
                
                if os.path.exists(destination_item_path):
                    logging.warning("File already exists in destination: %s. Overwriting.", destination_item_path)
                    if os.path.isdir(source_item_path):
                        shutil.rmtree(destination_item_path)
                    else:
                        os.remove(destination_item_path)
                
                shutil.move(source_item_path, destination_path)
            
            # After merging, remove the original source folder
            os.rmdir(booth_folder_path)
            logging.info("Merged and removed original folder: %s", booth_folder_path)
        else:
            shutil.move(booth_folder_path, cleanup_folder)
            logging.info("Moved folder to cleanup: %s", booth_folder_path)
    
    except Exception as e:
        logging.error("Error moving to cleanup: %s", str(e))


def delete_empty_folders(download_folder):
    logging.info("Deleting empty folders in: %s", download_folder)
    for root, dirs, files in os.walk(download_folder):
        for dir_name in dirs:
            dir_path = os.path.join(root, dir_name)
            if not os.listdir(dir_path):
                try:
                    os.rmdir(dir_path)
                    logging.info("Deleted empty folder: %s", dir_path)
                except OSError as e:
                    logging.error("Failed to delete folder: %s. Error: %s", dir_path, str(e))

import os
import re
import json
import logging
import requests
import yaml
from bs4 import BeautifulSoup
import newflattener  # Import the flattener module

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger()

# Function to sanitize filenames by replacing invalid characters with underscores
def sanitize_filename(filename):
    invalid_chars = r'[<>:"/\\|?*\x00-\x1F\x7F]'
    sanitized_filename = re.sub(invalid_chars, '_', filename)
    return sanitized_filename

# Function to extract the 7-digit code from the booth.pm URL
def extract_code(booth_url):
    match = re.search(r'\d{7}$', booth_url)
    return match.group(0) if match else None

# Function to create a Crawljob file
def create_crawljob(download_url, booth_url, watch_folder, download_folder, folder_number, folder_mapping):
    code = extract_code(booth_url)
    if not code:
        logger.error(f"Invalid booth URL: {booth_url}")
        return

    target_folder = folder_mapping.get(str(folder_number), "OTHER")
    download_path = os.path.join(download_folder, target_folder, code)

    crawljob = {
        "text": download_url,
        "packageName": f"download_{code}",
        "autoStart": True,
        "enabled": True,
        "extractAfterDownload": False,
        "forcedStart": True,
        "downloadFolder": download_path
    }

    job_filename = os.path.join(watch_folder, f"download_{code}.crawljob")
    with open(job_filename, 'w') as job_file:
        for key, value in crawljob.items():
            job_file.write(f"{key}={json.dumps(value)}\n")
    
    logger.info(f"Created Crawljob: {job_filename}")
    thumbnail_path = os.path.join(download_path, f"{code}.jpg")
    download_thumbnail(booth_url, thumbnail_path)

# Function to download the thumbnail from booth.pm
def download_thumbnail(booth_url, dest_path):
    response = requests.get(booth_url)
    response.raise_for_status()
    soup = BeautifulSoup(response.content, 'html.parser')

    booth_name = None
    meta_tag = soup.find("meta", {"name": "twitter:title"})
    if meta_tag:
        booth_name = meta_tag.get("content", "")
        booth_name = booth_name.split(" - BOOTH")[0]
        booth_name = booth_name.split("【")[-1].split("】")[0]
    else:
        logger.warning("No meta tag found in HTML file.")

    booth_id = extract_code(booth_url)
    if not booth_id:
        logger.error("Failed to extract booth ID.")
        return

    og_image_meta = soup.find('meta', property='og:image')
    if og_image_meta and og_image_meta['content']:
        img_url = og_image_meta['content']
        logger.info(f"Thumbnail URL: {img_url}")
        img_response = requests.get(img_url, stream=True)
        img_response.raise_for_status()
        os.makedirs(os.path.dirname(dest_path), exist_ok=True)
        with open(dest_path, 'wb') as f:
            for chunk in img_response.iter_content(chunk_size=8192):
                f.write(chunk)
        logger.info(f"Downloaded thumbnail to {dest_path}")

        if booth_name:
            sanitized_booth_name = sanitize_filename(booth_name)
            new_thumbnail_name = f"{sanitized_booth_name}_{booth_id}.jpg"
            new_thumbnail_path = os.path.join(os.path.dirname(dest_path), new_thumbnail_name)
            os.rename(dest_path, new_thumbnail_path)
            logger.info(f"Renamed thumbnail to {new_thumbnail_path}")
        else:
            logger.warning("Failed to extract booth name. Thumbnail will not be renamed.")
        return

    logger.error(f"No valid image found in {booth_url}")

# Function to clear the console screen
def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

# Function to display the folder mapping on the screen
def display_folder_mapping(folder_mapping):
    print("Folder Mapping:")
    for number, name in folder_mapping.items():
        print(f"{number}: {name}")

# Function to process links
def process_links(watch_folder, download_folder, folder_mapping, organize):
    file_path = os.path.join(os.path.dirname(__file__), 'links.txt')
    if not os.path.exists(file_path):
        with open(file_path, 'w') as file:
            pass
        logger.info(f"Created empty file: {file_path}")

    while True:
        print("Choose an option:")
        print("1. Enter links manually")
        print("2. Use links from 'links.txt' file")
        choice = input("Enter 1 or 2: ").strip()

        if choice == '1':
            while True:
                if organize:
                    clear_console()
                    display_folder_mapping(folder_mapping)
                download_url = input("Enter the download link (or '1' to quit): ")
                if download_url.lower() == '1':
                    break
                booth_url = input("Enter the booth.pm link: ")
                folder_number = None
                if organize:
                    folder_number = input("Enter the folder number: ")
                create_crawljob(download_url, booth_url, watch_folder, download_folder, folder_number, folder_mapping)
            break
        
        elif choice == '2':
            with open(file_path, 'r') as file:
                lines = file.readlines()
            
            if len(lines) % 3 != 0:
                logger.error(f"Invalid format in 'links.txt'. Please ensure there are sets of three lines per download.")
                return
            
            download_count = len(lines) // 3
            print(f"There are {download_count} downloads to be processed.")
            while True:
                print("1. Start downloading")
                print("2. Cancel")
                start_choice = input("Enter 1 or 2: ").strip()

                if start_choice == '1':
                    for i in range(0, len(lines), 3):
                        download_url = lines[i].strip()
                        booth_url = lines[i + 1].strip()
                        folder_number = lines[i + 2].strip() if organize else None
                        create_crawljob(download_url, booth_url, watch_folder, download_folder, folder_number, folder_mapping)
                    break
                elif start_choice == '2':
                    logger.info("Download cancelled by user.")
                    break
                else:
                    logger.error("Invalid choice. Please enter '1' or '2'.")
            break
        else:
            logger.error("Invalid choice. Please enter '1' or '2'.")

# Function to sanitize folder path
def sanitize_folder_path(folder_path):
    sanitized_path = folder_path.strip()
    sanitized_path = sanitized_path.rstrip("\\/")
    return sanitized_path

# Function to load configuration from YAML file
def load_config(config_path):
    if not os.path.exists(config_path):
        # Prompt for the watch folder
        while True:
            watch_folder = input("Enter the path to the watch folder: ").strip()
            if watch_folder and not watch_folder.isspace() and os.path.isdir(watch_folder):
                break
            else:
                print("Invalid folder path or folder does not exist. Please try again.")

        # Prompt for the download folder
        while True:
            download_folder = input("Enter the path to the download folder: ").strip()
            if download_folder and not download_folder.isspace() and os.path.isdir(download_folder):
                break
            else:
                print("Invalid folder path or folder does not exist. Please try again.")

        # Prompt for the cleanup folder (it may not exist yet)
        cleanup_folder = input("Enter the path to the cleanup folder (will be created if it doesn't exist): ").strip()

        config = {
            'watch_folder': watch_folder,
            'download_folder': download_folder,
            'cleanup_folder': cleanup_folder
        }

        # Save the new config to a file
        with open(config_path, 'w') as config_file:
            yaml.dump(config, config_file)
        logger.info(f"Created config file: {config_path}")

        return config

    else:
        # Load the existing config
        with open(config_path, 'r') as config_file:
            config = yaml.safe_load(config_file)

        # Validate or prompt for the watch folder
        if 'watch_folder' not in config or not config['watch_folder'] or config['watch_folder'].isspace() or not os.path.isdir(config['watch_folder']):
            while True:
                new_watch_folder = input("Enter the correct watch folder path: ").strip()
                if new_watch_folder and not new_watch_folder.isspace() and os.path.isdir(new_watch_folder):
                    config['watch_folder'] = new_watch_folder
                    break
                else:
                    print("Invalid folder path or folder does not exist. Please try again.")

        # Validate or prompt for the download folder
        if 'download_folder' not in config or not config['download_folder'] or config['download_folder'].isspace() or not os.path.isdir(config['download_folder']):
            while True:
                new_download_folder = input("Enter the correct download folder path: ").strip()
                if new_download_folder and not new_download_folder.isspace() and os.path.isdir(new_download_folder):
                    config['download_folder'] = new_download_folder
                    break
                else:
                    print("Invalid folder path or folder does not exist. Please try again.")

        # Validate or prompt for the cleanup folder (even if it doesn't exist yet)
        if 'cleanup_folder' not in config or not config['cleanup_folder'] or config['cleanup_folder'].isspace():
            cleanup_folder = input("Enter the path to the cleanup folder (will be created if it doesn't exist): ").strip()
            config['cleanup_folder'] = cleanup_folder

        # Save any updates back to the config file
        with open(config_path, 'w') as config_file:
            yaml.dump(config, config_file)

        return config

# Function to load folder mapping from JSON file
def load_folder_mapping(mapping_path):
    if os.path.exists(mapping_path):
        with open(mapping_path, 'r') as mapping_file:
            return json.load(mapping_file)
    else:
        logger.error(f"Folder mapping file '{mapping_path}' does not exist.")
        return {}
    
# Main function to run the script
def main():
    config_path = 'config.yaml'
    folder_mapping_path = 'folders.json'

    # Load configuration from config.yaml file or prompt user to enter paths
    config = load_config(config_path)

    # Load folder mapping from folders.json file
    folder_mapping = load_folder_mapping(folder_mapping_path)

    watch_folder = config['watch_folder']
    download_folder = config['download_folder']
    cleanup_folder = config['cleanup_folder']
    
    # Clear the console before processing links
    clear_console()
    organize = input("Do you want to organize downloads into folders? (1-yes, 2-no): ").strip() == '1'

    process_links(watch_folder, download_folder, folder_mapping, organize)

    # After processing links, ask if user wants to run the flattener
    run_flattener = input("Do you want to run the flattener? (1-yes, 2-no): ").strip() == '1'

    # If user chose to organize downloads and wants to run the flattener, proceed
    if run_flattener:
        newflattener.process_folders(download_folder, folder_mapping,cleanup_folder, organize)
        newflattener.delete_empty_folders(download_folder)

    logger.info("Script execution completed.")

if __name__ == "__main__":
    main()
